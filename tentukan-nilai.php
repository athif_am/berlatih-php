<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tentukan Nilai</title>
</head>
<body>
    <h1>Tentukan Nilai</h1>
    <?php
    function tentukan_nilai($number){
    //  kode disini
    if ($number > 80) {
        echo $number . " => Sangat Baik <br>";
    }
    elseif ($number > 70) {
        echo $number . " => Baik <br>";
    }
    elseif ($number > 50) {
        echo $number . " => Cukup <br>";
    }
    elseif ($number < 50) {
        echo $number . " => Kurang <br>";
    }
}

//TEST CASES
tentukan_nilai(98); //Sangat Baik
tentukan_nilai(76); //Baik
tentukan_nilai(67); //Cukup
tentukan_nilai(43); //Kurang
?>

</body>
</html>