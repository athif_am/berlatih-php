<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ubah Huruf</title>
</head>
<body>
<h1>Ubah Huruf</h1>
<?php

function ubah_huruf($str){
//kode di sini
$huruf  = "abcdefghijklmnopqrsuvwxyz";
$out  ="";
for ($i = 0; $i < strlen($str); $i++){
    $position = strrpos($huruf, $str[$i]);
    $out .= substr($huruf, $position + 1, 1);
}

return $out;
}   

// TEST CASES
echo ubah_huruf('wow') . "<br>"; // xpx
echo ubah_huruf('developer') . "<br>"; // efwfmpqfs
echo ubah_huruf('laravel') . "<br>"; // mbsbwfm
echo ubah_huruf('keren') . "<br>"; // lfsfo
echo ubah_huruf('semangat') . "<br>"; // tfnbohbu

?>

</body>
</html>